import { configureStore } from "@reduxjs/toolkit";
import tasksReducer from "./storiesSlice";

// This is the root state of the app
export default configureStore({
  reducer: {
    stories: tasksReducer,
  },
});
