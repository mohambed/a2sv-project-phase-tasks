import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import allStories from "../components/stories.json";
import { IStory } from "../components/StoryItem";

interface LikeStoryPayload {
  id: number;
}

interface StoriesState {
  stories: IStory[];
}

export const storiesSlice = createSlice({
  name: "stories",
  initialState: { stories: [] } as StoriesState,
  reducers: {
    fetchStories: (state) => {
      const { stories } = allStories;

      for (let i = 0; i < stories.length; i++) {
        state.stories.push({ ...stories[i], id: i + 1, likes: 0 });
      }
    },
    likeStory: (state, action: PayloadAction<LikeStoryPayload>) => {
      const { id } = action.payload;
      const story = state.stories.find((story) => story.id === id);

      if (story) {
        story.likes++;
      }
    },
  },
});

export const { likeStory, fetchStories } = storiesSlice.actions;

export default storiesSlice.reducer;
