import React from "react";
import { MdOutlineKeyboardArrowUp } from "react-icons/md";

const Scroller = () => {
  return (
    <a
      href="#home"
      className="fixed bottom-4 right-4 scroll-smooth rounded-full bg-blue-600 p-5 transition-all hover:bg-blue-500"
    >
      <MdOutlineKeyboardArrowUp className="scale-150 transform text-white" />
    </a>
  );
};

export default Scroller;
