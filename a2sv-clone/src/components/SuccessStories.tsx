import React from "react";
import Intro from "./Intro";
import StoriesList from "./StoriesList";

const SuccessStories = () => {
  return (
    <section
      id="home"
      className="flex flex-col items-center justify-center bg-gray-50"
    >
      <Intro />
      <StoriesList />
    </section>
  );
};

export default SuccessStories;
