import React from "react";
import { AiOutlineMail } from "react-icons/ai";
import {
  BsLinkedin,
  BsInstagram,
  BsFacebook,
  BsTwitter,
  BsYoutube,
} from "react-icons/bs";

const Footer = () => {
  return (
    <footer className="flex h-52 flex-col items-center justify-center bg-gray-50">
      <h3 className="mb-7 text-xl font-medium text-gray-600">
        Follow us on social media
      </h3>
      <div className="flex items-center justify-between space-x-10">
        <div className="cursor-pointer rounded-full bg-gray-200 p-3 hover:bg-gray-300">
          <AiOutlineMail className="h-6 w-6 text-red-600" />
        </div>
        <div className="cursor-pointer rounded-full bg-gray-200 p-3 hover:bg-gray-300">
          <BsLinkedin className="h-6 w-6 text-blue-500" />
        </div>
        <div className="cursor-pointer rounded-full bg-gray-200 p-3 hover:bg-gray-300">
          <BsInstagram className="h-6 w-6 text-purple-700" />
        </div>
        <div className="cursor-pointer rounded-full bg-gray-200 p-3 hover:bg-gray-300">
          <BsFacebook className="h-6 w-6 text-blue-500" />
        </div>
        <div className="cursor-pointer rounded-full bg-gray-200 p-3 hover:bg-gray-300">
          <BsTwitter className="h-6 w-6 text-blue-400" />
        </div>
        <div className="cursor-pointer rounded-full bg-gray-200 p-3 hover:bg-gray-300">
          <BsYoutube className="h-6 w-6 text-red-600" />
        </div>
      </div>
      <hr className="mb-2 mt-5 h-2 w-[98%] fill-gray-200" />
      <h3 className="text-sm font-thin">
        © Copyright 2023{" "}
        <span className="font-extrabold">A2SV Foundation.</span> All rights
        reserved.
        <span className="font-bold text-blue-600">
          {" "}
          Privacy Policy | Cookie Policy
        </span>
      </h3>
    </footer>
  );
};

export default Footer;
