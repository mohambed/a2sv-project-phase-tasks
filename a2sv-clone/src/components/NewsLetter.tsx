import React from "react";

const NewsLetter = () => {
  return (
    <div className="flex min-h-screen flex-col items-center justify-center bg-gradient-to-r from-blue-900 to-blue-600">
      <h1 className="mb-10 text-4xl font-extrabold text-white">
        Subscribe to our newsletter!
      </h1>
      <Input label="Email" isMandatory={true} />
      <Input label="First name" isMandatory={false} />
      <Input label="Last name" isMandatory={false} />
      <div className="mr-16 mt-2 flex items-center justify-between space-x-4">
        <input
          className="scale-150 transform"
          type="checkbox"
          name="checkbox"
          id="checkbox"
        />
        <label htmlFor="checkbox" className="text-sm text-white">
          Check if related to A2SV as a student, mentor or other
        </label>
      </div>
      <button className="mt-7 w-[29rem] rounded-xl bg-yellow-500 py-3 font-semibold text-white">
        Subscribe Now
      </button>
    </div>
  );
};

const Input: React.FC<{ label: string; isMandatory: boolean }> = ({
  label,
  isMandatory,
}) => {
  return (
    <div className="mb-4 flex flex-col items-start justify-between">
      <label className="mb-3 text-white" htmlFor={label}>
        {label}
        {isMandatory && <span className="text-red-400">*</span>}
      </label>
      <input
        className="w-[29rem] rounded-xl bg-gray-300 p-3"
        type="text"
        name={label}
        id={label}
      />
    </div>
  );
};

export default NewsLetter;
