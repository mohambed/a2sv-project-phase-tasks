import React from "react";
import { LiaCookieBiteSolid } from "react-icons/lia";

const CookiesButton = () => {
  return (
    <button className="fixed bottom-4 left-4 rounded-full bg-blue-800 p-4 transition-all hover:bg-blue-500">
      <LiaCookieBiteSolid className="scale-150 transform text-white" />
    </button>
  );
};

export default CookiesButton;
