import React from "react";
import ReactApexChart from "react-apexcharts";

const Intro = () => {
  const chart = {
    series: [
      {
        data: [2, 27, 59, 70],
      },
    ],
    options: {
      chart: {
        type: "bar",
        height: 350,
      },
      plotOptions: {
        bar: {
          borderRadius: 4,
          horizontal: true,
        },
      },
      dataLabels: {
        enabled: false,
      },
      yaxis: {
        categories: ["0%", "20%", "40%", "60%", "80%", "100%"],
      },
    },
  };

  return (
    <div className="flex min-h-screen flex-col items-center justify-center">
      <h1 className="mb-8 mt-36 text-5xl font-bold">Success Stories</h1>
      <h3 className="mb-12 text-2xl">
        A2SV has turned the dreams of African software engineers into a reality
        and continues to thrive.
      </h3>
      <h5>Google SWE Interviews Acceptance Rate Comparison</h5>
      <h4 className="text-xl">
        A2SV students are 35 times more likely to pass Google SWE interviews
        than average candidates.
      </h4>
      <div className="mb-32 mt-6">
        <ReactApexChart
          className="test"
          //@ts-ignore
          options={chart.options}
          series={chart.series}
          type="bar"
          width="900"
          height="400"
        />
      </div>
    </div>
  );
};

export default Intro;
