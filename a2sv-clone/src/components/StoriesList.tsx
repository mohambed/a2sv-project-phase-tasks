import React, { useEffect } from "react";
import StoryItem, { IStory } from "./StoryItem";
import { useSelector, useDispatch } from "react-redux";
import { fetchStories } from "../redux/storiesSlice";

const StoriesList = () => {
  const { stories } = useSelector((state: any) => state.stories);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchStories());
  }, [dispatch]);

  return (
    <div className="flex flex-col items-center justify-between">
      <h1 className="text-5xl font-bold">Impact Stories</h1>
      <h3 className="mt-4 text-2xl">
        Behind every success is a story. Learn about the stories of A2SVians.
      </h3>
      {stories.map((story: IStory) => (
        <StoryItem story={story} key={story.id} />
      ))}
    </div>
  );
};

export default StoriesList;
