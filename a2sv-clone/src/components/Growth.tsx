import React from "react";
import ReactApexChart from "react-apexcharts";

const Growth = () => {
  const chart = {
    series: [
      {
        name: "sales",
        data: [
          {
            x: "2019/01/01",
            y: 400,
          },
          {
            x: "2019/04/01",
            y: 430,
          },
          {
            x: "2019/07/01",
            y: 448,
          },
          {
            x: "2019/10/01",
            y: 470,
          },
          {
            x: "2020/01/01",
            y: 540,
          },
          {
            x: "2020/04/01",
            y: 580,
          },
          {
            x: "2020/07/01",
            y: 690,
          },
          {
            x: "2020/10/01",
            y: 690,
          },
        ],
      },
    ],
    options: {
      chart: {
        type: "bar",
        height: 380,
      },
      xaxis: {
        type: "category",
        labels: {},
        group: {
          style: {
            fontSize: "10px",
            fontWeight: 700,
          },
          groups: [
            { title: "2019", cols: 4 },
            { title: "2020", cols: 4 },
          ],
        },
      },
      title: {
        text: "The Growth of A2SV",
      },
      tooltip: {},
    },
  };

  return (
    <div className="flex min-h-screen items-center justify-center bg-gray-50">
      <ReactApexChart
        //@ts-ignore
        options={chart.options}
        series={chart.series}
        type="bar"
        height={380}
        width={1000}
      />
    </div>
  );
};

export default Growth;
