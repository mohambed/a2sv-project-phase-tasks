import React from "react";
import { FcLike } from "react-icons/fc";
import { useDispatch } from "react-redux";
import { likeStory } from "../redux/storiesSlice";

export interface IStory {
  image: string;
  name: string;
  title: string;
  company: string;
  contents: { heading: string; content: string }[];
  id: number;
  likes: number;
}

const StoryItem: React.FC<{ story: IStory }> = ({ story }) => {
  const dispatch = useDispatch();

  const onLike = () => {
    dispatch(likeStory({ id: story.id }));
  };

  return (
    <div className="relative my-6 flex max-w-[67rem] flex-col items-center justify-between space-x-8 rounded-md p-10 shadow-xl md:flex-row">
      <div className="relative min-h-max min-w-max">
        <img src={story.image} alt={story.name} className="w-96 rounded-md" />
        <div className="absolute bottom-4 left-4">
          <h3 className="text-xl font-extrabold text-white">{story.name}</h3>
          <h4 className="text-md font-semibold text-white">{story.title}</h4>
          <h3 className="text-md font-bold text-white">{story.company}</h3>
        </div>
      </div>
      <div className="flex flex-col justify-between space-y-4">
        {story.contents.map((content) => {
          return (
            <div>
              <h3 className="mb-2 text-xl font-bold">{content.heading}</h3>
              <p className="text-md italic">{content.content}</p>
            </div>
          );
        })}
      </div>
      <button
        className="absolute bottom-4 right-4 flex items-center justify-center space-x-2 rounded-md p-1 ring ring-pink-300 transition-all hover:bg-pink-100"
        onClick={onLike}
      >
        <FcLike />
        <span className="text-pink-600">{story.likes} likes</span>
      </button>
    </div>
  );
};

export default StoryItem;
