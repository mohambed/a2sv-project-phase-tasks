import React from "react";
import { BsFillHeartFill } from "react-icons/bs";

const SideButton = () => {
  return (
    <button className="fixed -left-14 top-[45%] flex -rotate-90 transform items-center  justify-between space-x-3 rounded-b-md border-2 border-black bg-blue-600 px-5 py-3 shadow-md transition-all hover:bg-blue-500">
      <BsFillHeartFill className="animate-ping text-white" />
      <span className="text-md font-bold text-white">Make a Gift</span>
    </button>
  );
};

export default SideButton;
