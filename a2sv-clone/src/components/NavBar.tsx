import React from "react";
import { MdOutlineKeyboardArrowDown } from "react-icons/md";

const NavBar = () => {
  return (
    <nav className="fixed z-50 flex w-full items-stretch justify-between bg-white shadow-sm">
      <img
        src="https://a2sv.org/logos/logo-blue.png"
        alt="A2SV logo"
        className="my-2 ml-16 h-14"
      />
      <ul className="flex items-center">
        <li className="text-md flex h-full min-w-max cursor-pointer items-center justify-center px-4 hover:bg-gray-100">
          Home
        </li>
        <li className="text-md group relative flex h-full min-w-max cursor-pointer items-center justify-center space-x-2 px-4 hover:bg-gray-100">
          <span>Teams</span>
          <MdOutlineKeyboardArrowDown />
          <DropdownMenu />
        </li>
        <li className="text-md relative flex h-full min-w-max cursor-pointer items-center justify-center px-4 hover:bg-gray-100">
          <span>Success Stories</span>
          <span className="absolute bottom-0 left-0 right-0 h-1.5 rounded-t-md bg-blue-600"></span>
        </li>
        <li className="text-md flex h-full min-w-max cursor-pointer items-center justify-center px-4 hover:bg-gray-100">
          About Us
        </li>
        <li className="text-md flex h-full min-w-max cursor-pointer items-center justify-center px-4 hover:bg-gray-100">
          Get Involved
        </li>
      </ul>
      <button className="my-4 mr-14 rounded-md bg-blue-600 px-6 py-2 font-medium tracking-wider text-white transition-all hover:bg-blue-500">
        Donate
      </button>
    </nav>
  );
};

const DropdownMenu = () => {
  return (
    <ul className="group-hover absolute top-[80%] z-30 hidden h-72 w-40 rounded-md bg-gray-200 shadow-xl group-hover:block">
      <li className="p-3 font-medium hover:bg-gray-300">Board Members</li>
      <li className="p-3 font-medium hover:bg-gray-300">Advisors/Mentors</li>
      <li className="p-3 font-medium hover:bg-gray-300">Executives</li>
      <li className="p-3 font-medium hover:bg-gray-300">Staff</li>
      <li className="p-3 font-medium hover:bg-gray-300">Placements</li>
      <li className="p-3 font-medium hover:bg-gray-300">Groups</li>
    </ul>
  );
};

export default NavBar;
