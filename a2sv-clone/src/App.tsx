import React from "react";
import "./App.css";
import NavBar from "./components/NavBar";
import SuccessStories from "./components/SuccessStories";
import Footer from "./components/Footer";
import NewsLetter from "./components/NewsLetter";
import Scroller from "./components/Scroller";
import CookiesButton from "./components/CookiesButton";
import Growth from "./components/Growth";
import SideButton from "./components/SideButton";

function App() {
  return (
    <div className="relative">
      <NavBar />
      <SuccessStories />
      <Growth />
      <NewsLetter />
      <Footer />
      <Scroller />
      <CookiesButton />
      <SideButton />
    </div>
  );
}

export default App;
