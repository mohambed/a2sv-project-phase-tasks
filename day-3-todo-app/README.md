# Day 3 Task: Todo App

## Getting Started

- clone the repo

```bash
git clone <repo-url>
```

- Install dependencies

```bash
cd <repo-name>
npm install
```

- Run the app

```bash
npm start
```

## Screenshots

### List of Todos

This part has been redesigned to allow for user to mark a todo as completed or not completed.
<img src="./screenshots/new.png"/>

<br>

### Add Todo

<br>
<img src="./screenshots/add-1.png"/>
<br>

<img src="./screenshots/add-2.png"/>
<br>

### Delete Todo

<br>
<img src="./screenshots/delete-1.png"/>
<br>

<img src="./screenshots/delete-2.png"/>
<br>

### Edit Todo

<br>

<img src="./screenshots/edit-1.png"/>
<br>

<img src="./screenshots/edit-2.png"/>
```

# Task 2: React Router

### Home Page

<br>

<img src="./screenshots/router-1.png"/>
<br>

### Add Page

<br>

<img src="./screenshots/router-2.png"/>
