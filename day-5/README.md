# Blog app

## Home page

![Home page](./screenshots/home.png)
<br>

## Details page

![Details page](./screenshots/details-1.png)

<br>

## Editing blog
   
![Details page](./screenshots/details-2.png)

<br>

## Creating a new blog

![Create new blog](./screenshots/create.png)