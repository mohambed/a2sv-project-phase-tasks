import React from "react";
import axios from "axios";
import { Blog } from "@/types";

const Blog: React.FC<{ blog: Blog }> = ({ blog }) => {
  const [title, setTitle] = React.useState(blog.title);
  const [body, setBody] = React.useState(blog.body);
  const [isEditing, setIsEditing] = React.useState(false);

  const editHandler = () => {
    if (!isEditing) {
      setIsEditing(true);
      return;
    }
    const updatedBlog = {
      ...blog,
      title,
      body,
    };

    axios(`https://jsonplaceholder.typicode.com/posts/${blog.id}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      data: JSON.stringify(updatedBlog),
    }).then(() => {
      alert("Blog updated");
      setIsEditing(false);
    });
  };

  return (
    <div className="min-h-screen bg-gray-700 pl-24 pt-24">
      <input
        className={`mb-8 block bg-transparent text-4xl font-bold capitalize text-indigo-500
        ${isEditing && " ring-2 ring-indigo-500"}`}
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        disabled={!isEditing}
      />
      <textarea
        className={`w-max bg-transparent text-lg text-gray-300 ${
          isEditing && " ring-2 ring-indigo-500"
        }`}
        onChange={(e) => setBody(e.target.value)}
        disabled={!isEditing}
        value={body}
        cols={58}
        rows={6}
      />
      <div className="mt-6 flex max-w-xl items-start justify-start space-x-6">
        <span className="rounded-md p-2 text-lg font-semibold text-indigo-400 ring-2 ring-gray-700">
          ID: {blog.id}
        </span>
        <span className="rounded-md p-2 text-lg font-semibold text-indigo-400 ring-2 ring-gray-700">
          UserID: {blog.userId}
        </span>
      </div>
      <button
        className="text-md mt-4 w-[10rem] rounded-md bg-green-500 px-4 py-2 font-semibold text-white transition-all hover:bg-green-600 hover:ring-2 hover:ring-green-500 focus:outline-none focus:ring-2 focus:ring-green-500"
        onClick={editHandler}
      >
        {isEditing ? "Save" : "Edit"}
      </button>
    </div>
  );
};

export const getServerSideProps = async (context: any) => {
  const res = await axios.get(
    `https://jsonplaceholder.typicode.com/posts/${context.params.id}`,
  );

  return {
    props: {
      blog: res.data,
    },
  };
};

export default Blog;
