import React from "react";
import axios from "axios";

const Create = () => {
  const [title, setTitle] = React.useState("");
  const [body, setBody] = React.useState("");

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const blog = {
      title,
      body,
      id: Math.random() * 1000,
      userId: Math.random() * 1000,
    };

    axios("https://jsonplaceholder.typicode.com/posts", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      data: JSON.stringify(blog),
    }).then(() => {
      alert("New blog added");
      setTitle("");
      setBody("");
    });
  };

  return (
    <div className="min-h-screen bg-gray-700 pl-11 pt-24">
      <form
        className="flex max-w-md flex-col items-center justify-between space-y-6 rounded-md py-6 ring-8 ring-gray-600"
        onSubmit={handleSubmit}
      >
        <div className="flex flex-col items-start justify-between space-y-2">
          <label htmlFor="title" className="text-white">
            Blog Title
          </label>
          <input
            type="text"
            name="title"
            id="title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            className="text-md min-w-[18rem] rounded-md bg-gray-800 px-2 py-1.5 font-medium text-white focus:outline-none focus:ring-2 focus:ring-indigo-500"
          />
        </div>
        <div className="flex flex-col items-start justify-between space-y-2">
          <label htmlFor="body" className="text-white">
            Blog Body
          </label>
          <textarea
            name="body"
            id="body"
            cols={32}
            rows={5}
            value={body}
            onChange={(e) => setBody(e.target.value)}
            className="text-md rounded-md bg-gray-800 px-2 py-1.5 font-medium text-white focus:outline-none focus:ring-2 focus:ring-indigo-500"
          />
        </div>
        <input
          className="text-md w-[10rem] rounded-md bg-indigo-500 px-4 py-2 font-semibold text-white transition-all hover:bg-indigo-600 hover:ring-2 hover:ring-indigo-500 focus:outline-none focus:ring-2 focus:ring-indigo-500"
          type="submit"
        />
      </form>
    </div>
  );
};

export default Create;
