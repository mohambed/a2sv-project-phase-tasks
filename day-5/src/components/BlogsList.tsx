import { Blog } from "@/types";
import React from "react";
import Link from "next/link";

const BlogsList: React.FC<{ blogs: Blog[] }> = ({ blogs }) => {
  return (
    <>
      {blogs.map((blog: Blog) => (
        <Link
          href={`/blogs/${blog.id}`}
          key={blog.id}
          className="flex min-h-[14rem] max-w-[30rem] flex-col items-center justify-between space-y-4 rounded-xl bg-gray-800 p-4 outline-none transition-all hover:bg-indigo-950"
        >
          <h1 className="text-2xl font-semibold capitalize text-indigo-400">
            {blog.title}
          </h1>
          <p className="text-md text-gray-300">{blog.body}</p>
        </Link>
      ))}
    </>
  );
};

export default BlogsList;
