import React from "react";
import Link from "next/link";
import { MdArrowBack } from "react-icons/md";
import { BiMessageAdd } from "react-icons/bi";
import { useRouter } from "next/router";

const NavBar = () => {
  const currentPath = useRouter().pathname;

  const navBarDetails = new Map([
    [
      "/",
      {
        text: "All Blogs",
        icon: <BiMessageAdd className="text-3xl" />,
        linkTo: "Create a new Blog",
      },
    ],
    [
      "/create",
      {
        text: "Create a new Blog",
        icon: <MdArrowBack className="text-3xl" />,
        linkTo: "All Blogs",
      },
    ],
    [
      "*",
      {
        text: "Blog Details",
        icon: <MdArrowBack className="text-3xl" />,
        linkTo: "All Blogs",
      },
    ],
  ]);

  const { text, icon, linkTo } =
    navBarDetails.get(currentPath) || navBarDetails.get("*");

  return (
    <div className="flex items-center justify-between space-x-8 bg-gray-800 px-64 py-6">
      <h1 className="text-4xl font-semibold text-white">{text}</h1>
      <Link
        href={currentPath === "/" ? "/create" : "/"}
        className="flex items-center justify-between space-x-4 rounded-lg px-4 py-2 text-white ring-2 ring-gray-500
        transition-all hover:bg-gray-800 hover:text-gray-400 hover:ring-gray-400
        "
      >
        {icon}
        <span>{linkTo}</span>
      </Link>
    </div>
  );
};

export default NavBar;
