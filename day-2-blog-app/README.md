# Demo
<br/>
<br/>

1. Task 1 - Simple blog app
<img src="./task-1.png"/>

2. Task 2 - Added like and comment feature

<img src="./task-2.png"/>

  * Used the useState hook to store the state of the like button
  * Used the useState hook to store the comments of the post as an array of strings.
  * Used the map function to iterate over the comments array and display each comment in a new line.
  * Used the useState hook to store the state of the comment input field.
