import React from "react";

const Title: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  return <h2 className="text-md font-semibold text-gray-600">{children}</h2>;
};

export default Title;
