import React from "react";

const Date: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  return <span className="text-xs text-gray-500">{children}</span>;
};

export default Date;
