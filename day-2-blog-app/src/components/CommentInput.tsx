import React from "react";

type CommentInputProps = {
  onAddComment: (comment: string) => void;
};

const CommentInput: React.FC<CommentInputProps> = ({ onAddComment }) => {
  const [comment, setComment] = React.useState<string>("");

  const addComment = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onAddComment(comment);
    setComment("");
  };

  return (
    <form
      className="flex items-center justify-center space-x-8 p-4"
      onSubmit={addComment}
    >
      <input
        type="text"
        className="rounded-sm border-none bg-gray-100 px-1 py-1 outline-none ring-2 ring-gray-300 focus:ring-indigo-600"
        value={comment}
        onChange={(e) => setComment(e.target.value)}
        placeholder="Add a comment..."
      />
      <button className="rounded-md bg-indigo-500 px-4 py-1 text-white transition-all hover:bg-indigo-700">
        Add
      </button>
    </form>
  );
};

export default CommentInput;
