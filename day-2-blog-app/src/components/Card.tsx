import React from "react";
import Title from "./Title";
import Author from "./Author";
import Date from "./Date";
import { BsChatLeft } from "react-icons/bs";
import CommentInput from "./CommentInput";
import Comment from "./Comment";
import { BiLike } from "react-icons/bi";

// This is the type of the props that we are expecting to receive
type CardProps = {
  title: string;
  author: string;
  date: string;
  content?: string;
};

const Card: React.FC<CardProps> = ({ title, author, date, content }) => {
  const [comments, setComments] = React.useState<string[]>([]);
  const [isCommenting, setIsCommenting] = React.useState<boolean>(false);
  const [isLiked, setIsLiked] = React.useState<boolean>(false);
  const [likes, setLikes] = React.useState<number>(3);

  const onAddComment = (comment: string) => {
    setComments((prevComments) => [...prevComments, comment]);
  };

  const onLike = () => {
    setLikes((prevLikes) => prevLikes + 1);
    setIsLiked(true);
  };

  const onUnlike = () => {
    setLikes((prevLikes) => prevLikes - 1);
    setIsLiked(false);
  };

  return (
    <div className="max-w-[350px] rounded bg-gray-50">
      <img
        className="rounded-t"
        alt="crypto"
        src="https://source.unsplash.com/random?laptops&1"
      />
      <div className="p-6">
        <Title>{title}</Title>
        <div className="my-2 flex items-center justify-start space-x-2">
          <Author name={author} />
          <Date>{date}</Date>
        </div>
        <div className="flex flex-col items-start justify-center space-y-3">
          <div className="flex items-start justify-center space-x-2">
            <span className="rounded-full bg-slate-100 px-2 py-0.5 text-xs font-medium uppercase text-gray-600 shadow-md ring-1 ring-gray-400">
              UI/UX
            </span>
            <span className="rounded-full bg-slate-100 px-2 py-0.5 text-xs font-medium uppercase text-gray-600 shadow-md ring-1 ring-gray-400">
              Development
            </span>
          </div>
          {/* Here we are using the content prop to render the content of the card if it exists, otherwise we are rendering a default message */}
          <p className="text-sm text-gray-600">
            {content ? content : "Under construction..."}
          </p>
          <hr className="w-full flex-grow border-gray-400" />
          <div className="flex w-full items-center justify-between">
            <div className="flex items-center justify-center space-x-2">
              <BiLike
                size={22}
                className={`hover:cursor-pointer ${
                  isLiked ? "text-blue-500" : "text-gray-500"
                }`}
                onClick={isLiked ? () => onUnlike() : () => onLike()}
              />
              <span className="text-sm font-medium text-gray-600">
                {likes} Likes
              </span>
            </div>
            <div className="flex items-center justify-center space-x-3">
              <BsChatLeft className="text-gray-500" />

              <button
                className="border-none text-sm font-medium text-indigo-600 transition-all hover:text-indigo-800"
                onClick={() =>
                  setIsCommenting((prevIsCommenting) => !prevIsCommenting)
                }
              >
                {isCommenting
                  ? "Hide Comments"
                  : `Add Comment (${comments.length})`}
              </button>
            </div>
          </div>
        </div>
      </div>
      {isCommenting && (
        <>
          <hr className="w-full flex-grow border-gray-400" />
          <CommentInput onAddComment={onAddComment} />
          <hr className="w-full flex-grow border-gray-400" />
          <div className="max-h-[150px] overflow-y-auto bg-indigo-100">
            {comments.map((comment, index) => (
              <Comment key={index} comment={comment} />
            ))}
          </div>
        </>
      )}
    </div>
  );
};

export default Card;
