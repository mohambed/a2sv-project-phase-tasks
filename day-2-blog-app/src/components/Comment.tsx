import React from "react";

type CommentProps = {
  comment: string;
};

const Comment: React.FC<CommentProps> = ({ comment }) => {
  return (
    <div className="pl-4">
      <p className="m-2 rounded-md bg-white p-2 text-xs font-thin">
        {comment}
      </p>
    </div>
  );
};

export default Comment;
