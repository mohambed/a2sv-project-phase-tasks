import React from "react";

const Author: React.FC<{ name: string }> = ({ name }) => {
  return (
    <div className="flex items-center justify-start space-x-1">
      <img
        className="mr-2 h-10 w-10 rounded-full"
        src="https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=600"
        alt="profile"
      />
      <span className="text-xs text-gray-500">by</span>
      <span className="text-xs font-semibold text-gray-00">{name}</span>
    </div>
  );
};

export default Author;
