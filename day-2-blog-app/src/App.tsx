import React from "react";
import "./App.css";
import Card from "./components/Card";
import allCards from "./components/cards.json";

function App() {
  // Here we are destructuring the cards array from the cards.json file
  const { cards } = allCards;

  return (
    <div className="flex min-h-screen items-center justify-between bg-gray-800 p-12">
      {/* Here we are mapping over the cards array and passing each card object as props to the Card component */}
      {cards.map((card) => (
        <Card key={card.id} {...card} />
      ))}
    </div>
  );
}

export default App;
