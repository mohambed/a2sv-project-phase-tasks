import axios from "axios";

const BASE_URL = "https://jsonplaceholder.typicode.com";

const fetchAllPosts = async () => {
  try {
    const allPosts = await axios.get(`${BASE_URL}/posts`);
    console.log(allPosts.data);
  } catch (error) {
    console.log(error);
  }
};

// fetchAllPosts();

const fetchPostById = async (id: number) => {
  try {
    const post = await axios.get(`${BASE_URL}/posts/${id}`);
    console.log(post.data);
  } catch (error) {
    console.log(error);
  }
};

// fetchPostById(1);




const fetchPostByUserId = async (userId: number) => {
  try {
    const post = await axios.get(`${BASE_URL}/posts?userId=${userId}`);
    console.log(post.data);
  } catch (error) {
    console.log(error);
  }
};

// fetchPostByUserId(1);




const addPost = async (post: any) => {
  try {
    const newPost = await axios.post(`${BASE_URL}/posts`, post);
    console.log(newPost.data);
  } catch (error) {
    console.log(error);
  }
};

// addPost({
//   userId: 200,
//   id: 200,
//   title: "New Post",
//   body: "New Post Body",
// });

// using headers
const addPostWithHeaders = async (post: any) => {
  try {
    const newPost = await axios.post(`${BASE_URL}/posts`, post, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    console.log(newPost.data);
  } catch (error) {
    console.log(error);
  }
};

addPostWithHeaders({
  userId: 200,
  id: 200,
  title: "New Post",
  body: "New Post Body",
});
